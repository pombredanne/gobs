GOBS - build system and package manager for go language.
========================================================

System description.
-------------------

Many program languages have tools for managing dependencies. For example, _java_ developers
use _maven_ or _ivy_. There's a tool _npm_ for _node.js_. And so on.

_Go_-language have a tool _go_ for it. However, it has some disadvantages:

  * There is no document, describing project dependencies.
  * _go get_ tool uses compiler version to select appropriate dependency version,
    but not version required by project.
  * Uses system variable _GOPATH_ as project root.

_GOBS_ system attempts to solve this problems.

### Quick start.

First of all, you need to install the _gobs_ tool:

    go get bitbucket.org/vegansk/gobs

If your system variable _PATH_ contains _$GOPATH/bin_, then you can use the ___gobs___ command from
your shell. Let's make a "Hello, world!" application. The first step is to create the project.
Execute following command in desired directory:

    gobs create -version 1.0 -license GPLv3 -desc "My first GOBS application" hw

Then change directory to the project sources:

    cd hw/src/hw

Add a dependency to the project by creating _deps_ object in _goproject.json_:

    {
        "name": "hw",
        "version": "1.0",
        "description": "My first GOBS application",
        "license": "GPLv3",

        "deps": {
            "bitbucket.org/vegansk/gobs/examplelib": "*"
        }
    }

Resolve dependencies with command:

    gobs resolve

Create a file named hw.go with the following content:

    package main

    import "bitbucket.org/vegansk/gobs/examplelib"

    func main() {
        examplelib.SayHello("World")
    }

Build project using command:

    gobs build

Cd to binary directory and execute project:

    cd ../../bin && ./hw

You should see the message in the console:

    Hello, world!

### Project structure.

_GOBS_ use standard directory structure for project, as described [here](http://golang.org/doc/code.html#Workspaces).

There are two files, related to _GOBS_:

  * ___.goproject___, located in the root of the workspace. Contains path to current project in _src_ directory.
  * ___goproject.json___, located in the root of the project's sources.


