package main

import (
    "bitbucket.org/vegansk/gobsgen"
    "os"
    "os/exec"
    "path/filepath"
    "strings"
)

var cmdBuild = &Command{
    Name:        "build",
    Description: "Build project",
    HelpText:    buildHelp,
}

const buildHelp = `
"build" command.
================

This command is used to build project. It is similar to "go install project_name"
command.

Usage:

    build [options]

, where options inherited from go build command. See help on "go build" command.

There is option alias -debug, expanded to -gcflags "-N -l" for better debugging

`

func updateIfRequired(proj *Project) {
    found := false
    for dep, tag := range proj.Deps {
        path := filepath.Join(proj.RootDir, "src", strings.Replace(dep, "/", string(filepath.Separator), -1))
        if isPathExists(path) {
            continue
        }
        resolveSingleDependency(dep, tag, proj)
        found = true
    }
    if found {
        doInstall(proj)
    }
}

func expandDebugFlag(args []string) (res []string) {
    res = make([]string, 0, len(args))
    for _, arg := range args {
        switch arg {
        case "-debug":
            res = append(res, "-gcflags", "-N -l")
        default:
            res = append(res, arg)
        }
    }
    return
}

func build(args []string) {
    args = expandDebugFlag(args)

    proj := mustLoadProject()

    updateIfRequired(proj)

    if proj.Templates {
        srcDir := filepath.Join(proj.RootDir, "src")
        err := gobsgen.GenerateFromSources(srcDir, proj.SrcDir)
        if err != nil {
            panic(err)
        }
    }

    buildArgs := []string{"install"}
    buildArgs = append(buildArgs, args...)
    if buildArgs[len(buildArgs)-1] != proj.FullName {
        buildArgs = append(buildArgs, proj.FullName)
    }

    cmd := exec.Command("go", buildArgs...)
    cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
    cmd.Env = proj.CreateEnv().slice()
    err := cmd.Run()
    if err != nil {
        panic(err)
    }
}

func init() {
    cmdBuild.Executor = build
}
