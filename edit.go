package main

import (
    "flag"
    "fmt"
    "os"
    "os/exec"
)

var cmdEdit = &Command{
    Name:        "edit",
    Description: "Start editing project",
    HelpText:    editHelp,
}

const editHelp = `
"edit" command.
===============

Start editing current project.

Usage:

    edit [-e editor]

, where editor is one of:
  * vim
  * gvim (default)

`

var (
    flEditor string // Editor type
)

const vimrcTemplate = `source ~/.vimrc

:autocmd BufNewFile,BufRead *.go set makeprg=gobs\ build
:autocmd BufNewFile,BufRead *.got set ft=go
`

func createVimrc(proj *Project) (fd *os.File) {
    fd = mustCreateTempFile()
    defer fd.Close()
    renderTemplateToWriter(fd, vimrcTemplate, proj)
    return
}

func createEditorParams(editor string, proj *Project) (cmd string, params []string) {
    switch editor {
    case "vim", "gvim":
        cmd = editor
        fd := createVimrc(proj)
        params = append(params, "-u", fd.Name(),
            "--cmd", fmt.Sprintf("cd %s", proj.RootDir))
    default:
        showMessageWithUsageAndExit(fmt.Sprintf("Unknown editor %s", flEditor), cmdEdit)
    }
    return
}

func edit(args []string) {
    proj := mustLoadProject()
    c, params := createEditorParams(flEditor, proj)

    env := proj.CreateEnv()
    cmd := exec.Command(c, params...)
    cmd.Env = env.slice()
    cmd.Stdin = os.Stdin
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr
    err := cmd.Run()
    if err != nil {
        showErrorAndExit(err)
    }
}

func init() {
    cmdEdit.Executor = edit

    var f = flag.NewFlagSet("edit", flag.ContinueOnError)
    f.StringVar(&flEditor, "e", "gvim", "")
    f.Usage = getUsageFunc(cmdEdit)

    cmdEdit.Flags = f
}
