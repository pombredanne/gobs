package examplelib

import "fmt"

func SayHello(forWhom string) {
    fmt.Printf("Hello, %s!\n", forWhom)
}
