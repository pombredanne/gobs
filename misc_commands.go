package main

import (
    "os"
    "os/exec"
)

var cmdClean = &Command{
    Name:        "clean",
    Description: "Clean the project",
    HelpText:    cleanProjectHelp,
}

const cleanProjectHelp = `
"clean" command.
================

This command is similar to _go clean -i_ call for current project.
`

func cleanProject(args []string) {
    proj := mustLoadProject()
    cmd := exec.Command("go", "clean", "-i", proj.FullName)
    cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
    if err := cmd.Run(); err != nil {
        showErrorAndExit(err)
    }
}

func init() {
    cmdClean.Executor = cleanProject
}
