package main

import (
    "bytes"
    "encoding/json"
    "flag"
    "fmt"
    "io"
    "io/ioutil"
    "os"
    "path/filepath"
    "strings"
)

const (
    PROJECT_FILE      = "goproject.json" // Project file name
    PROJECT_INFO_FILE = ".goproject"     // File with current project's info
    VIMRC_FILE        = ".vimrc"
    GITIGNORE_FILE    = ".gitignore"
    GITKEEP_FILE      = ".gitkeep"
)

type Dependencies map[string]string
type Installs []string

// Project description
type Project struct {
    Name        string       `json:"name"`                  // Project name
    Version     string       `json:"version"`               // Project version
    Description string       `json:"description,omitempty"` // Project description
    License     string       `json:"license,omitempty"`     // Project license
    Templates   bool         `json:"templates,omitempty"`   // Use templates in project
    Deps        Dependencies `json:"deps,omitempty"`        // Project dependencies
    Install     Installs     `json:"install,omitempty"`     // What to install after resolving dependencies

    // Non JSON fields
    RootDir  string `json:"-"` // Root directory of the project
    SrcDir   string `json:"-"` // Source directory of the project
    FullName string `json:"-"` // Full project name (subdirectory in src directory)
}

// Get dependency's directory
func (p *Project) GetDependencyDir(dep string) (res string) {
    parts := strings.Split(dep, "/")
    return filepath.Join(append([]string{p.RootDir, "src"}, parts...)...)
}

// Save project's JSON to writer
func (p *Project) Save(writer io.Writer) error {
    data, err := json.MarshalIndent(p, "", "  ")
    if err != nil {
        return err
    }
    _, err = writer.Write(data)
    return err
}

func (p *Project) MustSaveProject() {
    // Create project file
    fd := mustCreateFile(filepath.Join(p.SrcDir, PROJECT_FILE))
    defer fd.Close()
    err := p.Save(fd)
    if err != nil {
        showErrorAndExit(err)
    }

    // Create project info file
    fd = mustCreateFile(filepath.Join(p.RootDir, PROJECT_INFO_FILE))
    defer fd.Close()
    _, err = fd.WriteString(p.FullName)
    if err != nil {
        showErrorAndExit(err)
    }

    // Try to save license
    if text, ok := knownLicense[p.License]; ok {
        fd = mustCreateFile(filepath.Join(p.SrcDir, p.License))
        defer fd.Close()
        _, err = fd.WriteString(text)
        if err != nil {
            showErrorAndExit(err)
        }
    }
}

// Load project from JSON
func (p *Project) Load(reader io.Reader) error {
    dec := json.NewDecoder(reader)
    return dec.Decode(p)
}

// Find project root and project directory
func findProject() (rootDir string, projectDir string) {
    var projInfoFile string
    for rootDir = mustGetwd(); rootDir != "." && rootDir != "/"; rootDir = filepath.Dir(rootDir) {
        projInfoFile = filepath.Join(rootDir, PROJECT_INFO_FILE)
        if isPathExists(projInfoFile) {
            break
        }
        projInfoFile = ""
    }

    if projInfoFile == "" {
        showErrorAndExit(fmt.Errorf("There is not project file in %s or it's parents", mustGetwd()))
    }

    fd := mustOpenFile(projInfoFile)
    defer fd.Close()

    data, err := ioutil.ReadAll(fd)
    if err != nil {
        showErrorAndExit(err)
    }

    projectName := strings.TrimSpace(string(data))
    projectDir = filepath.Join(rootDir, "src", projectName)
    projectFile := filepath.Join(projectDir, PROJECT_FILE)

    if !isPathExists(projectFile) {
        showErrorAndExit(fmt.Errorf("No project found in %s", projectDir))
    }

    return
}

// Load project from current directory or panic
func mustLoadProject() *Project {
    root, src := findProject()
    fd, err := os.Open(filepath.Join(src, PROJECT_FILE))
    if err != nil {
        showErrorAndExit(fmt.Errorf("Can't load project. Error: %v", err))
    }
    p := new(Project)
    if err = p.Load(fd); err != nil {
        showErrorAndExit(fmt.Errorf("Error loading project: %v", err))
    }
    p.RootDir, p.SrcDir, p.FullName = root, src, src[len(root)+5:]
    return p
}

func (p *Project) CreateEnv() Env {
    if p.RootDir == "" {
        showErrorAndExit(fmt.Errorf("Project not loaded"))
    }
    e := newEnv()
    e["GOPATH"] = p.RootDir
    e["PATH"] = fmt.Sprintf("%s%c%s", filepath.Join(p.RootDir, "bin"), filepath.ListSeparator, e["PATH"])
    return e
}

var createProjectHelp = `
"create" command.
=================

Usage:

    create [flags] project_name

This command is used to create a new project. Without flags, you will
need to answer a few questions about your project:

 * project's version
 * short description of the project
 * license type of the project

After that, it will create a directory structure for the project and the
file called "goproject.json" with project's description.

It may be usefull to set the parameters of the project via command line.
The flags are:

    -version
        Set version of the project
    -desc
        Set description of the project
    -license
        Set license of the project. Must be the name of the license
        file or one of: {{.}}.
    -path projectPath
        Override project path. If not specified, gobs will use a
        subdirectory, named like the project, in the current directory.

`

const initProjectHelp = `
"init" command.
===============

This command is used to initialize existing project or to activate
current project. 

Usage:

    init [path]
    
Command must be executed from project's source directory, or the path
must be specified as a parameter.
`

var cmdCreateProject = &Command{
    Name:        "create",
    Description: "Create new project",
}

var cmdInitProject = &Command{
    Name:        "init",
    Description: "Initialize existing project",
    HelpText:    initProjectHelp,
}

var (
    flName        string
    flVersion     string
    flDescription string
    flLicense     string
    flPath        string
)

const gitignoreTemplate = `# No swap files
.*.swp
`

func createProjectInternal(rootDir, name, version, desc, license string) {
    srcDir := filepath.Join(rootDir, "src", name)
    p := Project{
        Name:        filepath.Base(name),
        Version:     version,
        Description: desc,
        License:     license,
        RootDir:     rootDir,
        SrcDir:      srcDir,
        FullName:    name,
    }

    projectFile := filepath.Join(srcDir, PROJECT_FILE)
    if isPathExists(projectFile) {
        panic(fmt.Errorf("Existing project found in %s!", srcDir))
    }
    mustCreateDir(srcDir)

    p.MustSaveProject()

    // Create directory structure
    subdirs := []string{"bin", "pkg"}
    for _, dir := range subdirs {
        mustCreateDir(filepath.Join(rootDir, dir))
    }

    // Create .gitignore
    fd := mustCreateFile(filepath.Join(srcDir, GITIGNORE_FILE))
    defer fd.Close()
    renderTemplateToWriter(fd, gitignoreTemplate, &p)
}

func createProject(args []string) {
    if len(args) != 1 {
        showMessageWithUsageAndExit("No project name given", cmdCreateProject)
    }

    var projectDir string
    if flPath == "" {
        pwd, err := os.Getwd()
        if err != nil {
            panic(err)
        }
        projectDir = filepath.Join(pwd, args[0])
    } else {
        projectDir = flPath
    }
    createProjectInternal(projectDir, args[0], flVersion, flDescription, flLicense)
}

func initProject(args []string) {
    var srcDir string
    var err error
    switch {
    case len(args) > 1:
        showMessageWithUsageAndExit("Too many parameters", cmdInitProject)
    case len(args) == 1:
        srcDir, err = filepath.Abs(args[0])
        if err != nil {
            showErrorAndExit(err)
        }
    default:
        srcDir = mustGetwd()
    }

    projectFile := filepath.Join(srcDir, PROJECT_FILE)
    if !isPathExists(projectFile) {
        showMessageWithUsageAndExit(fmt.Sprintf("Project not found in %s", srcDir), cmdInitProject)
    }

    paths := make(StringStack, 0, 2)
    rootDir := srcDir
    base := ""
    for rootDir != "/" && rootDir != "." {
        base = filepath.Base(rootDir)
        rootDir = filepath.Dir(rootDir)
        if base == "src" {
            break
        }
        paths.Push(base)
        base = ""
    }
    if base == "" {
        showErrorAndExit(fmt.Errorf("Base directory not found for project in %s", srcDir))
    }

    projectName := ""
    for n, ok := paths.Pop(); ok != false; n, ok = paths.Pop() {
        if len(projectName) > 0 {
            projectName += string(filepath.Separator)
        }
        projectName += n
    }

    fd := mustCreateFile(filepath.Join(rootDir, PROJECT_INFO_FILE))
    defer fd.Close()
    if _, err := fd.WriteString(projectName); err != nil {
        showErrorAndExit(err)
    }
}

func init() {
    cmdCreateProject.Executor = createProject
    f := flag.NewFlagSet("create", flag.ContinueOnError)
    f.Usage = getUsageFunc(cmdCreateProject)
    f.StringVar(&flVersion, "version", "", "")
    f.StringVar(&flPath, "path", "", "")
    f.StringVar(&flDescription, "desc", "", "")
    f.StringVar(&flLicense, "license", "", "")

    cmdCreateProject.Flags = f
    var b bytes.Buffer
    renderTemplateToWriter(&b, createProjectHelp, strings.Join(getKnownLicenses(), ", "))
    cmdCreateProject.HelpText = string(b.Bytes())

    cmdInitProject.Executor = initProject
}
