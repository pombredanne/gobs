package main

import (
    "os"
    "os/exec"
)

var cmdShell = &Command{
    Name:        "shell",
    Description: "Run shell for project",
    HelpText:    shellHelp,
}

const shellHelp = `
"shell" command
===============

This command starts a shell configured for current project.
It has no parameters.

Currently only BASH shell is supported.

`

const bashrcTemplate = `
. ~/.bashrc

export PATH={{.RootDir}}/bin:$PATH
export GOPATH={{.RootDir}}
export PS1="[{{.Name}}] $PS1"

_gobs()
{
    local COMMANDS="help create init tags edit shell resolve build clean"
    local CREATE_KEYS="-version -desc -license -path"
    local BUILD_KEYS="-debug"
    local TAGS_KEYS="-name"
    local EDIT_KEYS="-e"
    local EDITORS="vim gvim"
    local cur=${COMP_WORDS[COMP_CWORD]}
    local prev=${COMP_WORDS[COMP_CWORD-1]}

    case "$prev" in
        create)
            COMPREPLY=( $(compgen -W "$CREATE_KEYS" -- $cur) )
            return 0
            ;;
        tags)
            COMPREPLY=( $(compgen -W "$TAGS_KEYS" -- $cur) )
            return 0
            ;;
        init)
            COMPREPLY=( $(compgen -d -o nospace -S / -- $cur) )
            return 0
            ;;
        edit)
            COMPREPLY=( $(compgen -W "$EDIT_KEYS" -- $cur) )
            return 0
            ;;
        build)
            COMPREPLY=( $(compgen -W "$BUILD_KEYS" -- $cur) )
            return 0
            ;;
        -e)
            COMPREPLY=( $(compgen -W "$EDITORS" -- $cur) )
            return 0
            ;;
        help)
            COMPREPLY=( $(compgen -W "$COMMANDS" -- $cur) )
            return 0
            ;;
        *)
            if [ $COMP_CWORD -eq 1 ]
            then
                COMPREPLY=( $(compgen -W "$COMMANDS" -- $cur) )
                return 0
            fi
            ;;
    esac
}

complete -F _gobs gobs
`

func startShell(args []string) {
    proj := mustLoadProject()

    rcFd := mustCreateTempFile()
    defer rcFd.Close()

    renderTemplateToWriter(rcFd, bashrcTemplate, proj)
    rcFd.Close()

    var shellArgs []string
    shellArgs = append(shellArgs, "--rcfile", rcFd.Name(), "-i")
    shellArgs = append(shellArgs, args...)
    cmd := exec.Command("bash", shellArgs...)
    cmd.Stdin = os.Stdin
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr

    err := cmd.Run()
    if err != nil {
        showErrorAndExit(err)
    }
}

func init() {
    cmdShell.Executor = startShell
}
