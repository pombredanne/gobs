package main

import (
    "flag"
    "fmt"
    "io"
    "io/ioutil"
    "os"
    "os/exec"
    "path/filepath"
    "regexp"
)

var cmdTags = &Command{
    Name:        "tags",
    Description: "Build project tag database with gotags",
    HelpText:    tagsHelp,
}

var tagsHelp = `
"tags" command
==============

This command is used to generate tags database for project.

Usage:

    tags [-name filename]

, where filename is the name of tags database. By default it will be
equal to "gotags".

`

var (
    flTagsName string
)

func generateFileList(w io.Writer, dir, re, reIgnore string) {
    regExp := regexp.MustCompile(re)
    var regExpIgnore *regexp.Regexp
    if reIgnore != "" {
        regExpIgnore = regexp.MustCompile(reIgnore)
    }
    entries, err := ioutil.ReadDir(dir)
    if err != nil {
        showErrorAndExit(err)
    }

    var subDirs []string
    for _, entry := range entries {
        if entry.IsDir() && entry.Name() != "." && entry.Name() != ".." {
            subDirs = append(subDirs, filepath.Join(dir, entry.Name()))
        } else {
            if regExp.FindString(entry.Name()) != "" && (reIgnore == "" || regExpIgnore.FindString(entry.Name()) == "") {
                if _, err := fmt.Fprintln(w, filepath.Join(dir, entry.Name())); err != nil {
                    showErrorAndExit(err)
                }
            }
        }
    }

    for _, subDir := range subDirs {
        generateFileList(w, subDir, re, reIgnore)
    }
}

func tags(args []string) {
    proj := mustLoadProject()
    fd := mustCreateTempFile()
    defer fd.Close()
    generateFileList(fd, proj.RootDir, "\\.go$", "")
    goRoot := os.ExpandEnv("$GOROOT")
    if proj.RootDir != goRoot {
        generateFileList(fd, filepath.Join(goRoot, "src", "pkg"), "\\.go$", "_test\\.go$")
    }
    tagsFile := filepath.Join(proj.RootDir, flTagsName)
    fdOut := mustCreateFile(tagsFile)
    defer fdOut.Close()
    cmd := exec.Command("gotags", "-L", fd.Name())
    cmd.Stdout = fdOut
    err := cmd.Run()
    if err != nil {
        showErrorAndExit(err)
    }
}

func init() {
    cmdTags.Executor = tags

    var f = flag.NewFlagSet("tags", flag.ContinueOnError)
    f.Usage = getUsageFunc(cmdTags)
    f.StringVar(&flTagsName, "name", "gotags", "")

    cmdTags.Flags = f
}
