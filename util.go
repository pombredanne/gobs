package main

import (
    "fmt"
    "io"
    "io/ioutil"
    "os"
    "strings"
    "text/template"
)

const (
    DIR_MODE = 0775 // Permissions for created directories
)

var OUT = os.Stdout

func exitProcess(code int) {
    deinitUtils()
    os.Exit(code)
}

func showMessageWithUsageAndExit(msg interface{}, cmd *Command) {
    fmt.Fprintf(OUT, "%v\n", msg)
    cmdHelp.Executor([]string{cmd.Name})
    exitProcess(-1)
}

func showErrorAndExit(err error) {
    fmt.Fprintf(OUT, "Error: %s\n\n", err.Error())
    exitProcess(-1)
}

func renderTemplateToWriter(w io.Writer, templ string, scope interface{}) {
    t := template.New("_")
    template.Must(t.Parse(templ))
    if err := t.Execute(w, scope); err != nil {
        panic(err)
    }
}

func renderTemplate(templ string, scope interface{}) {
    renderTemplateToWriter(OUT, templ, scope)
}

// Get working directory
func mustGetwd() string {
    wd, err := os.Getwd()
    if err != nil {
        showErrorAndExit(err)
    }
    return wd
}

// Create directory and all it's parents
func mustCreateDir(dir string) {
    if err := os.MkdirAll(dir, DIR_MODE); err != nil {
        showErrorAndExit(err)
    }
}

// Create file
func mustCreateFile(name string) *os.File {
    f, err := os.Create(name)
    if err != nil {
        showErrorAndExit(err)
    }
    return f
}

// Open file
func mustOpenFile(name string) *os.File {
    f, err := os.Open(name)
    if err != nil {
        showErrorAndExit(err)
    }
    return f
}

var tempFiles = make([]string, 0, 10)

// Create temp file
func mustCreateTempFile() *os.File {
    tmpDir := os.TempDir()
    fd, err := ioutil.TempFile(tmpDir, "gobs")
    if err != nil {
        showErrorAndExit(err)
    }
    tempFiles = append(tempFiles, fd.Name())
    return fd
}

// Check if file or directory exists
func isPathExists(path string) bool {
    _, err := os.Stat(path)
    if err == nil || !os.IsNotExist(err) {
        return true
    }
    return false
}

func deinitUtils() {
    for _, f := range tempFiles {
        os.Remove(f)
    }
}

type Env map[string]string

func newEnv() (env Env) {
    env = make(Env)
    e := os.Environ()
    for _, s := range e {
        split := strings.SplitN(s, "=", 2)
        env[strings.TrimSpace(split[0])] = strings.TrimSpace(split[1])
    }
    return
}

func (e Env) slice() (res []string) {
    for k, v := range e {
        res = append(res, fmt.Sprintf("%s=%s", k, v))
    }
    return
}

type StringStack []string

func (s *StringStack) Push(v string) {
    *s = append(*s, v)
}

func (s *StringStack) Pop() (v string, ok bool) {
    l := len(*s)
    switch {
    case l > 0:
        v = (*s)[l-1]
        ok = true
        *s = (*s)[:l-1]
    default:
        ok = false
    }
    return
}
